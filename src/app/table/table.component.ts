import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import dataDummy from './data.json';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.styl']
})
export class TableComponent implements OnInit, AfterViewInit {
  
  @ViewChild(DataTableDirective)
  datatableElement: DataTableDirective;
  data: any;

  dtOptions: DataTables.Settings = {};

  constructor() {
    this.data = dataDummy
   }


  ngOnInit(): void {
    this.dtOptions = {
      data: this.data.plaza.postulados,
      columns: [{
        title: 'ID',
        data: 'id'
      }, {
        title: 'Nombre completo',
        data: 'nombre'
      }, {
        title: 'Codigo',
        data: 'codigo'
      }, {
        title: 'Estado',
        data: 'estado'
      }
    ]
    };
  }

  ngAfterViewInit(): void {
    let table = $("[data-table=estudiantespostulados]")
    this.datatableElement.dtInstance.then((dtInstance: DataTables.Api) => {
      
      this.data.estadosEstudiantes.forEach(estado => {
        $('#filter-estado').append(`<option value="${estado.nombre}">${estado.nombre}</option>`)
      });
      
      let filtersTr = $("<tr></tr>")
      
      $("#estudiantespostulados").children().each((i, field) => {
        filtersTr.append($('<th></th>').append(field))
      })
      
      table.find('thead')
        .append(filtersTr)

      table.find("thead tr:eq(1) th").each((i, th) => {
        const that = dtInstance.columns(i);
        $('input,select', th).on('keyup change', function () {
          console.log(this['value'])
          if (that.search() !== this['value']) {
            that
              .search(this['value'])
              .draw();
          }
        });

      })
      
    });
  }
}
